# https://sluteam.gitlab.io/buy-home-online/

# 關於 git

## Branch & merge request

1. 請自己開新的 branch，千萬不要更動 master。
2. 當要 **merge requests**，request 到 **dev** 分支，不要 request 到 ~~master~~ 分支。
3. 請參考 [A successful Git branching model](https://nvie.com/posts/a-successful-git-branching-model/)

# 規範

## 使用 Tailwind CSS
* [Tailwind CSS](https://tailwindcss.com/docs/guides/vue-3-vite)
1. 位置: scss/tailwinds.scss
2. 缺點: [tailwinds 把 h1, h2 ... 等都 unstyle 了。](https://tailwindcss.com/docs/preflight#headings-are-unstyled) [因此需要定義他們](https://github.com/tailwindlabs/tailwindcss/issues/1460)
3. 範例: 使用 tailwind 就跟 bootstrap 類似。 components/AppTailwindExamples.vue 中有 container 和 button 的例子。

## 把元素分開
1. 把 scss 檔放在 assests/scss 裡面。(請自己建立 ex: header.scss)
2. 把圖都放在 assets/images 裡面
* 使用 @/assets/scss/... 來 import 元素

## 請參考 Vue.js 提供的 coding style
* [coding style](https://v3.vuejs.org/style-guide/#priority-b-rules-strongly-recommended-improving-readability)
